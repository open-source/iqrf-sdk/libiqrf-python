"""Generic response messages."""

from .generic import GenericResponse

__all__ = (
    'GenericResponse',
)
