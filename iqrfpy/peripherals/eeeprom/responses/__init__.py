"""Eeeprom peripheral response messages."""

from .read import ReadResponse
from .write import WriteResponse

__all__ = (
    'ReadResponse',
    'WriteResponse',
)
