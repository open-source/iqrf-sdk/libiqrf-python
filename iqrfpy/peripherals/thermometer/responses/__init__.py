"""Thermometer peripheral response messages."""

from .read import ReadResponse

__all__ = (
    'ReadResponse',
)
