"""Thermometer peripheral request messages."""

from .read import ReadRequest

__all__ = (
    'ReadRequest',
)
