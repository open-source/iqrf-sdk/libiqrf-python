"""Enums module.

This module contains peripheral, command and message type enums.

Submodules
----------
:obj:`iqrfpy.enums.peripherals`: Peripherals module.\n
:obj:`iqrfpy.enums.commands`: Peripheral commands module.\n
:obj:`iqrfpy.enums.message_types`: Message types module.\n
"""
